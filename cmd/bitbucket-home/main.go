package main

import (
	"fmt"
	"log"
	"os/exec"
	"regexp"

	"github.com/pkg/browser"
)

func main() {
	url, err := exec.Command("git", "config", "--get", "remote.origin.url").Output()
	if err != nil {
		log.Fatal(err)
	}
	re := regexp.MustCompile(`@([\w-.]+):([\w/-]+)\/([\w-]+)\.`)
	matched := re.FindStringSubmatch(string(url))
	if len(matched) != 4 {
		log.Fatalf("Couldn't parse git remote url %s", url)
	}
	host, group, name := matched[1], matched[2], matched[3]
	err = browser.OpenURL(fmt.Sprintf("https://%s/%s/%s", host, group, name))
	if err != nil {
		log.Fatal(err)
	}
}
