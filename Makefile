golangci-lint = ./bin/golangci-lint
goreleaser = ./bin/goreleaser

# Start the app
run:
	@go run cmd/bitbucket-home/main.go
.PHONY: run

$(golangci-lint):
	curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s v1.21.0

$(goreleaser):
	curl -sfL https://install.goreleaser.com/github.com/goreleaser/goreleaser.sh | sh

# Lint the source code
lint: $(golangci-lint)
	@echo "Running golangci-lint..."
	@go list -f '{{.Dir}}' ./... \
		| xargs $(golangci-lint) run
.PHONY: lint

# Run all tests
test: lint
	@echo "Running go test..."
	@go test -cover ./...
.PHONY: test

# Release a new version
release: $(goreleaser)
	$(goreleaser) --skip-publish --rm-dist
.PHONY: release
