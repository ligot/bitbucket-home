# bitbucket-home

> Open the Bitbucket page of the current directory repo


## Install

The easiest way is to [download a binary](https://bitbucket.org/ligot/bitbucket-home/downloads/).

If you have the Go toolchain, you can also do

```
$ go get bitbucket.org/ligot/bitbucket-home/cmd/bitbucket-home
```


## Usage

```
$ bitbucket-home
```


## Tip

Add `alias bh=bitbucket-home` to your `.zshrc`/`.bashrc`, so that you can run it with `$ bh` instead.


## Credits

Heavily based on [gh-home](https://github.com/sindresorhus/gh-home)
